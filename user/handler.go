package user

import (
	"net/http"
	"strconv"

	"bodo-amat/trace"

	"github.com/allegro/bigcache/v3"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

func Handler(db *sqlx.DB, memory *bigcache.BigCache) *fiber.App {
	app := fiber.New()

	deps := &Dependency{
		DB:     db,
		Memory: memory,
	}

	app.Get("/id/:id", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		user, err := deps.Get(c.Context(), id)
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(user)
	})

	app.Get("/status/:status", func(c *fiber.Ctx) error {
		status, err := strconv.Atoi(c.Params("status"))
		if err != nil {
			return err
		}

		users, err := deps.GetStatus(c.Context(), status)
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(users)
	})

	app.Post("/test/:userid", func(c *fiber.Ctx) error {
		testID, err := uuid.NewRandom()
		if err != nil {
			return err
		}

		userID, err := uuid.Parse(c.Params("userid"))
		if err != nil {
			return err
		}

		var test Test
		err = c.BodyParser(&test)
		if err != nil {
			return err
		}
		test.ID = testID

		err = deps.AddTest(c.Context(), User{ID: userID}, test)
		if err != nil {
			return err
		}

		if test.Result == TestPositive {
			go trace.Warn(userID)
		}

		c.Status(http.StatusOK)
		return c.JSON(test)
	})

	return app
}
