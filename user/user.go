package user

import (
	"errors"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type User struct {
	ID          uuid.UUID `json:"id" db:"id" msgpack:"id"`
	Name        string    `json:"name" db:"name" msgpack:"name"`
	Email       string    `json:"email" db:"email" msgpack:"email"`
	Phone       int64     `json:"phone" db:"phone" msgpack:"phone"`
	Tests       []Test    `json:"tests" db:"tests" msgpack:"tests"`
	DateOfBirth time.Time `json:"date_of_birth" db:"date_of_birth" msgpack:"date_of_birth"`
	CreatedAt   time.Time `json:"created_at" db:"created_at" msgpack:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at" msgpack:"updated_at"`
}

type Test struct {
	ID         uuid.UUID `json:"id" db:"id" msgpack:"id"`
	Method     Method    `json:"method" db:"method" msgpack:"method"`
	Result     Result    `json:"result" db:"result" msgpack:"result"`
	ExpiryDate time.Time `json:"expiry_date" db:"expiry_date" msgpack:"expiry_date"`
	CreatedAt  time.Time `json:"created_at" db:"created_at" msgpack:"created_at"`
	UpdatedAt  time.Time `json:"updated_at" db:"updated_at" msgpack:"updated_at"`
}

type Dependency struct {
	DB     *sqlx.DB
	Memory *bigcache.BigCache
}

var ErrNotExists = errors.New("user does not exists")

type Result int
type Method int

const (
	TestPositive Result = iota
	TestNegative
)

const (
	TestAntigen Method = iota
	TestPCR
	TestRapid
)
