CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    name VARCHAR(256),
    email VARCHAR(256) UNIQUE,
    phone INTEGER UNIQUE,
    date_of_birth DATETIME,
    created_at DATETIME,
    updated_at DATETIME
);

CREATE TABLE IF NOT EXISTS tests (
    id UUID PRIMARY KEY,
    method INTEGER,
    result INTEGER,
    expiry_date DATETIME,
    created_at DATETIME,
    updated_at DATETIME,
    user_id UUID REFERENCES users(id)
);

CREATE INDEX IF NOT EXISTS t_result ON tests (result, expiry_date);
CREATE INDEX IF NOT EXISTS t_method ON tests (method);
CREATE INDEX IF NOT EXISTS t_user_id ON tests (user_id);
