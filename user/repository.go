package user

import (
	"context"
	"database/sql"
	"time"

	"github.com/google/uuid"
)

type Repo interface {
	Create(ctx context.Context, user User) error
	Get(ctx context.Context, id uuid.UUID) (User, error)
	GetAll(ctx context.Context) ([]User, error)
	GetStatus(ctx context.Context, status int) ([]User, error)
	Update(ctx context.Context, user User) error
	AddTest(ctx context.Context, user User, test Test) error
}

func (d *Dependency) Create(ctx context.Context, user User) error {
	// Check if the user exists
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO users
			(id, name, email, phone, date_of_birth, created_at, updated_at)
			VALUES
			($1, $2, $3, $4, $5, $6, $7)`,
		user.ID,
		user.Name,
		user.Email,
		user.Phone,
		user.DateOfBirth,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) Get(ctx context.Context, id uuid.UUID) (User, error) {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return User{}, err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return User{}, err
	}

	rows, err := tx.QueryxContext(ctx, `SELECT * FROM users WHERE id = $1`, id)
	if err != nil {
		tx.Rollback()
		return User{}, err
	}
	defer rows.Close()

	var user User
	for rows.Next() {
		err = rows.StructScan(&user)
		if err != nil {
			tx.Rollback()
			return User{}, err
		}
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return User{}, err
	}

	return user, nil
}

func (d *Dependency) GetAll(ctx context.Context) ([]User, error) {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return []User{}, err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return []User{}, err
	}

	rows, err := tx.QueryxContext(ctx, `SELECT * FROM users`)
	if err != nil {
		tx.Rollback()
		return []User{}, err
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var user User
		err = rows.StructScan(&user)
		if err != nil {
			tx.Rollback()
			return []User{}, err
		}
		users = append(users, user)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return []User{}, err
	}

	return users, nil
}

func (d *Dependency) GetStatus(ctx context.Context, status int) ([]User, error) {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return []User{}, err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return []User{}, err
	}

	rows, err := tx.QueryxContext(ctx, `SELECT * FROM users WHERE status = $1`, status)
	if err != nil {
		tx.Rollback()
		return []User{}, err
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var user User
		err = rows.StructScan(&user)
		if err != nil {
			tx.Rollback()
			return []User{}, err
		}
		users = append(users, user)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return []User{}, err
	}

	return users, nil
}

func (d *Dependency) Update(ctx context.Context, user User) error {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	r, err := tx.QueryxContext(ctx, `SELECT EXISTS(SELECT * FROM users WHERE id = $1)`, user.ID)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer r.Close()

	var exists bool
	for r.Next() {
		err = r.Scan(&exists)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if !exists {
		return ErrNotExists
	}

	_, err = tx.ExecContext(
		ctx,
		`UPDATE users
		SET name = $1,
			email = $2,
			phone = $3,
			date_of_birth = $4,
			updated_at = $5
		WHERE id = $6`,
		user.Name,
		user.Email,
		user.Phone,
		user.DateOfBirth,
		time.Now(),
		user.ID,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) AddTest(ctx context.Context, user User, test Test) error {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO tests
			(id, method, result, expiry_date, created_at, updated_at, user_id)
			VALUES
			($1, $2, $3, $4, $5, $6, $7)`,
		test.ID,
		test.Method,
		test.Result,
		test.ExpiryDate,
		time.Now(),
		time.Now(),
		user.ID,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}
