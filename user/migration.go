package user

import (
	"context"
	"database/sql"
)

func (d *Dependency) Migrate(ctx context.Context) error {
	c, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer c.Close()

	tx, err := c.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE TABLE IF NOT EXISTS users (
			id UUID PRIMARY KEY,
			name VARCHAR(256),
			email VARCHAR(256) UNIQUE,
			phone INTEGER UNIQUE,
			date_of_birth DATETIME,
			created_at DATETIME,
			updated_at DATETIME
		)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE TABLE IF NOT EXISTS tests (
			id UUID PRIMARY KEY,
			method INTEGER,
			result INTEGER,
			expiry_date DATETIME,
			created_at DATETIME,
			updated_at DATETIME,
			user_id UUID REFERENCES users(id)
		)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS t_result ON tests (result, expiry_date)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS t_method ON tests (method)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS t_user_id ON tests (user_id)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	err = tx.Commit()
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return nil
}
