package main

import (
	"log"
	"os"
	"time"

	"bodo-amat/auth"
	"bodo-amat/cico"
	"bodo-amat/location"
	"bodo-amat/user"

	"github.com/allegro/bigcache/v3"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

func main() {
	// Initialize Postgresql
	dbURL, err := pq.ParseURL(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}

	db, err := sqlx.Open("postgres", dbURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Initialize in memory datastore
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(time.Hour * 6))
	if err != nil {
		log.Fatal(err)
	}
	defer cache.Close()

	app := fiber.New()

	app.Mount("/location", location.Handler(db, cache))
	app.Mount("/user", user.Handler(db, cache))
	app.Mount("/cico", cico.Handler(db.DB))
	app.Mount("/user", user.Handler(db, cache))
	app.Mount("/auth", auth.Handler(db, cache))

	app.Listen(":" + os.Getenv("PORT"))
}
