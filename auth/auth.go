package auth

import (
	"bodo-amat/user"
	"time"

	"github.com/google/uuid"
)

type Authentication struct {
	ID        uuid.UUID `json:"id" db:"id" msgpack:"id"`
	Password  string    `json:"password" db:"password" msgpack:"password"`
	CreatedAt time.Time `json:"created_at" db:"created_at" msgpack:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at" msgpack:"updated_at"`
	Sessions  []Session `json:"sessions" db:"sessions" msgpack:"sessions"`
}

type Session struct {
	ID        uuid.UUID `json:"id" db:"id" msgpack:"id"`
	EvictDate time.Time `json:"evict_date" db:"evict_date" msgpack:"evict_date"`
	UserID    uuid.UUID `json:"-" db:"user_id" msgpack:"-"`
}

type RequestBody struct {
	user.User
	Password string `json:"password" db:"password" msgpack:"password"`
}
