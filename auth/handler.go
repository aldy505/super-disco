package auth

import (
	"github.com/allegro/bigcache/v3"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
)

func Handler(db *sqlx.DB, memory *bigcache.BigCache) *fiber.App {
	app := fiber.New()

	_ = &Dependency{
		DB:     db,
		Memory: memory,
	}

	app.Post("/login", func(c *fiber.Ctx) error {
		return nil
	})

	app.Post("/refresh", func(c *fiber.Ctx) error {
		return nil
	})

	app.Post("/register", func(c *fiber.Ctx) error {
		return nil
	})

	return app
}
