CREATE TABLE IF NOT EXISTS accounts (
    id UUID PRIMARY KEY,
    password TEXT,
    created_at DATETIME,
    updated_at DATETIME
);
