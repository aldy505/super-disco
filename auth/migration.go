package auth

import (
	"context"
	"database/sql"
)

func (d *Dependency) Migrate(ctx context.Context) error {
	c, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer c.Close()

	tx, err := c.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE TABLE IF NOT EXISTS accounts (
			id UUID PRIMARY KEY,
			password TEXT,
			created_at DATETIME,
			updated_at DATETIME
		)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	err = tx.Commit()
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return nil
}
