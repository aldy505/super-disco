package auth

import (
	"context"

	"github.com/allegro/bigcache/v3"
	"github.com/jmoiron/sqlx"
)

// TODO
type Repo interface {
	Register(ctx context.Context, body RequestBody) error
	Login(ctx context.Context, body RequestBody) error
	Refresh(ctx context.Context, token string) error
}

type Dependency struct {
	DB     *sqlx.DB
	Memory *bigcache.BigCache
}
