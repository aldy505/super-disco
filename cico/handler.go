package cico

import (
	"bodo-amat/location"
	"bodo-amat/user"
	"context"
	"database/sql"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

func Handler(db *sql.DB) *fiber.App {
	app := fiber.New()

	deps := &Dependency{
		DB: db,
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	err := deps.Migrate(ctx)
	if err != nil {
		panic(err)
	}

	app.Post("/in/id/:id", func(c *fiber.Ctx) error {
		locationID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		id, err := uuid.NewRandom()
		if err != nil {
			return err
		}

		data := CICO{
			ID: id,
			Location: location.Location{
				ID: locationID,
			},
			User: user.User{
				ID: c.Locals("userID").(uuid.UUID),
			},
		}

		err = deps.CheckIn(c.Context(), data)
		if err != nil {
			return err
		}

		return c.SendStatus(http.StatusOK)
	})

	app.Post("/out/id/:id", func(c *fiber.Ctx) error {
		locationID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		id, err := uuid.NewRandom()
		if err != nil {
			return err
		}

		data := CICO{
			ID: id,
			Location: location.Location{
				ID: locationID,
			},
			User: user.User{
				ID: c.Locals("userID").(uuid.UUID),
			},
		}

		err = deps.CheckOut(c.Context(), data)
		if err != nil {
			return err
		}

		return c.SendStatus(http.StatusOK)
	})

	return app
}
