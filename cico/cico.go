package cico

import (
	"bodo-amat/location"
	"bodo-amat/user"
	"database/sql"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/google/uuid"
)

type Dependency struct {
	DB     *sql.DB
	Memory *bigcache.BigCache
}

type CICO struct {
	ID           uuid.UUID         `json:"id" db:"id" msgpack:"id"`
	Event        Event             `json:"event" db:"event" msgpack:"event"`
	User         user.User         `json:"user" msgpack:"user"`
	Location     location.Location `json:"location" msgpack:"location"`
	AutoCheckout time.Time         `json:"auto_checkout" db:"auto_checkout" msgpack:"auto_checkout"`
	CreatedAt    time.Time         `json:"created_at" db:"created_at" msgpack:"created_at"`
}

type Event int

const (
	EventCheckIn Event = iota
	EventCheckOut
)
