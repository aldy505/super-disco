CREATE TABLE IF NOT EXISTS cico_events (
    id UUID PRIMARY KEY,
    event SMALLINT,
    user_id UUID REFERENCES users(id),
    location_id UUID REFERENCES locations(id),
    auto_checkout DATETIME,
    created_at DATETIME
);

CREATE INDEX IF NOT EXISTS c_time_loc ON cico_events (location_id, created_at);
CREATE INDEX IF NOT EXISTS c_event ON cico_events (event);
CREATE INDEX IF NOT EXISTS c_time_user ON cico_events (user_id, created_at);
