package cico

import (
	"context"
	"database/sql"
)

func (d *Dependency) Migrate(ctx context.Context) error {
	c, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer c.Close()

	tx, err := c.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE TABLE IF NOT EXISTS cico_events (
			id UUID PRIMARY KEY,
			event SMALLINT,
			user_id UUID REFERENCES users(id),
			location_id UUID REFERENCES locations(id),
			auto_checkout DATETIME,
			created_at DATETIME
		)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS c_time_loc ON cico_events (location_id, created_at)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS c_event ON cico_events (event)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS c_time_user ON cico_events (user_id, created_at)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	err = tx.Commit()
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return nil
}
