package cico

import (
	"context"
	"database/sql"
	"time"
)

type Repo interface {
	CheckIn(ctx context.Context, data CICO) error
	CheckOut(ctx context.Context, data CICO) error
}

func (d *Dependency) CheckIn(ctx context.Context, data CICO) error {
	conn, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO cico_events
			(id, event, user_id, location_id, created_at)
			VALUES
			($1, $2, $3, $4, $5)`,
		data.ID,
		EventCheckIn,
		data.User.ID,
		data.Location.ID,
		time.Now(),
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) CheckOut(ctx context.Context, data CICO) error {
	conn, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO cico_events
			(id, event, user_id, location_id, created_at)
			VALUES
			($1, $2, $3, $4, $5)`,
		data.ID,
		EventCheckOut,
		data.User.ID,
		data.Location.ID,
		time.Now(),
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}
