package location

// Setiap package ini adalah domain
// Domain merujuk kepada service terkecil pada suatu app.
// Jadi bisa tuh dibikin loosely-coupled.

import (
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Location struct {
	ID           uuid.UUID `json:"id" db:"id" msgpack:"id"`
	Name         string    `json:"name" db:"name" msgpack:"name"`
	Address      string    `json:"address" db:"address" msgpack:"address"`
	City         string    `json:"city" db:"city" msgpack:"city"`
	Phone        int64     `json:"phone" db:"phone" msgpack:"phone"`
	PeopleInside int32     `json:"people_inside" db:"people_inside" msgpack:"people_inside"`
	CreatedAt    time.Time `json:"created_at" db:"created_at" msgpack:"created_at"`
	UpdatedAt    time.Time `json:"updated_at" db:"updated_at" msgpack:"updated_at"`
}

type Dependency struct {
	DB     *sqlx.DB
	Memory *bigcache.BigCache
}

var ErrNotExists = errors.New("location id not found")
var ErrCantIncrement = errors.New("cant increment value by zero")
