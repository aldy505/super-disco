CREATE TABLE IF NOT EXISTS locations (
    id UUID PRIMARY KEY,
    name VARCHAR(256),
    address VARCHAR(256),
    city VARCHAR(256),
    phone INTEGER UNIQUE,
    people_inside INTEGER,
    created_at DATETIME,
    updated_at DATETIME
);

CREATE INDEX IF NOT EXISTS l_people_inside ON locations (people_inside);
