package location

import (
	"context"
	"database/sql"
)

func (d *Dependency) Migrate(ctx context.Context) error {
	c, err := d.DB.Conn(ctx)
	if err != nil {
		return err
	}
	defer c.Close()

	tx, err := c.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE TABLE IF NOT EXISTS locations (
			id UUID PRIMARY KEY,
			name VARCHAR(256),
			address VARCHAR(256),
			city VARCHAR(256),
			phone INTEGER UNIQUE,
			people_inside INTEGER,
			created_at DATETIME,
			updated_at DATETIME
		)`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`CREATE INDEX IF NOT EXISTS l_people_inside ON locations (people_inside);`,
	)
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	err = tx.Commit()
	if err != nil {
		if err = tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return nil
}
