package location

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack/v5"
)

type Repo interface {
	GetAll(ctx context.Context) ([]Location, error)
	Get(ctx context.Context, id uuid.UUID) (Location, error)
	Create(ctx context.Context, location Location) error
	Update(ctx context.Context, location Location) error
	UpdateVisitor(ctx context.Context, location Location, number int) error
	Delete(ctx context.Context, id uuid.UUID) error
}

func (d *Dependency) GetAll(ctx context.Context) ([]Location, error) {
	// Before we do anything to the database, we check the cache first.
	// If the cache is empty, we will populate it with the data from the database.
	// If the cache is not empty, we will use the data from the cache.

	data, err := d.Memory.Get("locations")
	if err == nil {
		var locations []Location
		err = msgpack.Unmarshal(data, &locations)
		if err != nil {
			return []Location{}, errors.Wrap(err, "failed unmarshalling the location data")
		}
		return locations, nil
	}

	// Fast return of the error if the error is not
	// entry not found.
	if !errors.Is(err, bigcache.ErrEntryNotFound) {
		return []Location{}, err
	}

	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return []Location{}, err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return []Location{}, err
	}

	rows, err := tx.QueryxContext(ctx, "SELECT * FROM locations")
	if err != nil {
		tx.Rollback()
		return []Location{}, err
	}
	defer rows.Close()

	var locations []Location
	for rows.Next() {
		var location Location
		err = rows.StructScan(&location)
		if err != nil {
			tx.Rollback()
			return []Location{}, err
		}

		locations = append(locations, location)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return []Location{}, err
	}

	compressed, err := msgpack.Marshal(locations)
	if err != nil {
		return []Location{}, errors.Wrap(err, "failed marshalling the location data")
	}

	err = d.Memory.Set("locations", compressed)
	if err != nil {
		return []Location{}, errors.Wrap(err, "failed writing to locations memory")
	}

	return locations, nil
}

func (d *Dependency) Get(ctx context.Context, id uuid.UUID) (Location, error) {
	var locations []Location

	data, err := d.Memory.Get("locations")
	if err == nil {
		err = msgpack.Unmarshal(data, &locations)
		if err != nil {
			return Location{}, errors.Wrap(err, "failed unmarshalling the location data")
		}

		// Find if there is a location data on the data variable
		for _, location := range locations {
			if location.ID == id {
				return location, nil
			}
		}
	}

	// Fast return of the error if the error is not
	// entry not found.
	if !errors.Is(err, bigcache.ErrEntryNotFound) {
		return Location{}, err
	}

	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return Location{}, err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return Location{}, err
	}

	rows, err := tx.QueryxContext(ctx, "SELECT * FROM locations WHERE id = $1", id)
	if err != nil {
		tx.Rollback()
		return Location{}, err
	}
	defer rows.Close()

	var location Location
	for rows.Next() {
		err = rows.StructScan(&location)
		if err != nil {
			tx.Rollback()
			return Location{}, err
		}
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return Location{}, err
	}

	return location, nil
}

func (d *Dependency) Create(ctx context.Context, location Location) error {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO locations
			(id, name, address, city, phone, created_at, updated_at)
			VALUES
			($1, $2, $3, $4, $5, $6, $7)`,
		location.ID,
		location.Name,
		location.Address,
		location.City,
		location.Phone,
		location.CreatedAt,
		location.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) Update(ctx context.Context, location Location) error {
	// Check if the location exists
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	r, err := tx.QueryxContext(ctx, `SELECT EXISTS(SELECT * FROM locations WHERE id = $1)`, location.ID)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer r.Close()

	var exists bool
	for r.Next() {
		err = r.Scan(&exists)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if exists {
		_, err = tx.ExecContext(
			ctx,
			`UPDATE locations
			SET name = $1,
				address = $2,
				city = $3,
				phone = $4,
				people_inside = $5,
				updated_at`,
			location.Name,
			location.Address,
			location.City,
			location.Phone,
			location.PeopleInside,
			time.Now(),
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) UpdateVisitor(ctx context.Context, location Location, number int) error {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	var query string
	if number > 0 {
		query = fmt.Sprintf(`UPDATE locations SET people_inside = people_inside + %d WHERE id = $1`, number)
	} else if number < 0 {
		query = fmt.Sprintf(`UPDATE locations SET people_inside = people_inside - %d WHERE id = $1`, number)
	} else {
		// number == 0
		tx.Rollback()
		return ErrCantIncrement
	}

	_, err = tx.ExecContext(
		ctx,
		query,
		location.ID,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (d *Dependency) Delete(ctx context.Context, id uuid.UUID) error {
	conn, err := d.DB.Connx(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	defer tx.Rollback()

	r, err := tx.QueryxContext(ctx, `SELECT EXISTS(SELECT * FROM locations WHERE id = $1`, id)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer r.Close()

	var exists bool
	for r.Next() {
		err = r.Scan(&exists)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if !exists {
		return ErrNotExists
	}

	_, err = tx.ExecContext(ctx, "DELETE FROM locations WHERE id = $1", id)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
