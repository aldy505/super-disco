package location

import (
	"net/http"

	"github.com/allegro/bigcache/v3"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

func Handler(db *sqlx.DB, cache *bigcache.BigCache) *fiber.App {
	app := fiber.New()

	deps := Dependency{
		DB:     db,
		Memory: cache,
	}

	app.Get("/", func(c *fiber.Ctx) error {
		locations, err := deps.GetAll(c.Context())
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(locations)
	})

	app.Get("/id/:id", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		location, err := deps.Get(c.Context(), id)
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(location)
	})

	app.Put("/", func(c *fiber.Ctx) error {
		var location Location
		err := c.BodyParser(&location)
		if err != nil {
			return err
		}

		location.ID = uuid.New()

		err = deps.Create(c.Context(), location)
		if err != nil {
			return err
		}

		c.Status(http.StatusCreated)
		return c.JSON(location)
	})

	app.Patch("/id/:id", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		var location Location
		err = c.BodyParser(&location)
		if err != nil {
			return err
		}

		location.ID = id

		err = deps.Update(c.Context(), location)
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(location)
	})

	app.Delete("/id/:id", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		err = deps.Delete(c.Context(), id)
		if err != nil {
			return err
		}

		c.Status(http.StatusOK)
		return c.JSON(id)
	})

	return app
}
