# API routes
- POST /auth/login - Just your usual user login, returns a JWT Token
- POST /auth/refresh - Just your usual refresh token
- POST /auth/register - Should I even explain what this is?
- POST /cico/in/id/{id} - id = location id
- POST /cico/out/id/{id} - id = location id
- GET /location - all locations
- GET /location/id/{id} - get a specific location's information by id
- PUT /location - add a new location
- PATCH /location/id/{id} - edit a location
- DELETE /location/id/{id} - you know
- POST /trigger - trigger a warning for the users that matches the request body
- POST /user/test - Submit a Covid-19 test result to a user
- GET /user/id/{id} - Get a user by their ID
- GET /user/status/{status} - Get list of users by their current status. Can be a "positive" and "negative", or "true" and "false"

# Schemas

## Auth

### Request body

Register:
```json
{
  "name": "string",
  "email": "string",
  "phone": 628000000,
  "date_of_birth": 10000000,
  "password": "string"
}
```

Login:
```json
{
  "email": "string",
  "phone": 62800000000,
  "password": "string"
}
```

### Response body

```json
{
  "token": "jwt token"
}
```

## Check in/out

### Request body

`Authorization` header with JWT token as value.

No JSON body needed.

### Response body

```json
{
  "people_inside": 100,
  "auto_checkout": 10000000
}
```

## Location

`Authorization` header with JWT token as value.

Anything will do. But the data needed to get it back is:

```json
{
  "id": "uuid",
  "name": "string",
  "address": "Sudirman Street",
  "phone": 622100000,
  "people_inside": 100
}
```

## Trigger warning

Can only be called internally by an internal service.

### Request body

```json
{
  "location_id": "uuid",
  "from_date": 10000000,
  "to_date": 10000000
}
```

## User

### Request body

Covid-19 test result:

```json
{
  "type": "antigen | pcr | rapid",
  "expiry_date": 10000000,
  "result": "positive | negative"
}
```

If the test result is positive, trigger a warning internally. Can be done through sending an internal HTTP request,
or just do it directly.

For the other user request, it's again up to you. But I want these:
- ID: uuid
- Name: string
- Phone: number
- Email: string
- Date of birth: unix time
- Test history: array of test history
- Location history: array of location history

Good luck!
